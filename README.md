# First Infra with terraform and verification with Chef Inspec

## This infra contain the follow features

- [x] A Default - VPC
- [x] A Default - Subnte
- [x] A default - Zone
- [x] Inside containe a instance type t2.micro and within are running nginx



![Map](https://gitlab.com/equinockx/Infra-Basic/-/blob/master/images/Untitled%20Diagram(1).png)


# Verify the infra with inspec

[Look the documentation](https://github.com/MoisesTapia/Infra-Basic/tree/master/infrabasic)
